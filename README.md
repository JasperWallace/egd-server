I used to have a Simtec [Entropy Key][0], but it died.

I got a couple of new hardware RNG's, a [oneRNG][1] , and a Wayward Geek [Infinite Noise TRNG][2]. Unfortunately neither has software for feeding entropy to virtual machines.

I did discover the virtio [RNG devices][3], but it doesn't seem to be straight forward to get them to work with my Xen PV setup.

The Simtec Entropy Key daemon will only feed entropy from an Entropy Key, but the protocol it uses is simple.

This is an implementation of the protocol so that existing ekey-egd-linux clients can now get some entropy.

Now adds a few more bits of the [EGD][4] protocol.

It just grabs data from /dev/urandom. If this annoys you send patches.

[0]: http://www.entropykey.co.uk/
[1]: http://onerng.info/
[2]: https://www.tindie.com/products/WaywardGeek/infinite-noise-true-random-number-generator/
[3]: http://wiki.qemu-project.org/Features/VirtIORNG
[4]: http://egd.sourceforge.net/

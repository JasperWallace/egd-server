// listen on ip:port
// accept connections
// read from connextions
// protocol is "\x02\x80" <- request rnd stuffs
// write 128 size blocks of random
// each client wants 4 blocks by default, range is 1-4
//
// 0x20x80 is EGD_CMD_BLOCKREAD, 128 bytes
//
//
//
// local EGD_CMD_QUERYPOOL = 0
//
// reutrns u32 of available entropy in bits
//
// local EGD_CMD_READBYTES = 1
//
// return u8 nbytes of entropy
//
// local EGD_CMD_BLOCKREAD = 2
//
// return u8 nbytes of entropy (blocking)
//
// local EGD_CMD_ADDENTROPY = 3
//
// Arguments: U16 shannons, U8 bytes, STR
//
// local EGD_CMD_GETPID = 4
//
// Return U8 slen, STR pidstr
//
//


use std::net::{TcpListener, TcpStream};
use std::thread;
use std::io::prelude::*;
use std::io::Cursor;
use std::{io, fs};
use std::fs::OpenOptions;
use std::str::FromStr;

extern crate libc;
extern crate byteorder;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

extern crate ipaddress;
use ipaddress::IPAddress;

#[macro_use]
extern crate clap;

#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate slog_async;
extern crate slog_syslog;

use slog::Drain;

const EGD_CMD_QUERYPOOL: u8 = 0;
const EGD_CMD_READBYTES: u8 = 1;
const EGD_CMD_BLOCKREAD: u8 = 2;
const EGD_CMD_ADDENTROPY: u8 = 3;
const EGD_CMD_GETPID: u8 = 4;

fn get_entropy() -> io::Result<u32> {
    let path = "/proc/sys/kernel/random/entropy_avail";

    fs::File::open(path)
        .map_err(|e| {
            io::Error::new(io::ErrorKind::Other,
                           format!("Can't find the entropy_avail entry in /proc: {}", e))
        })
        .and_then(|mut f| {
            let mut line = String::new();
            f.read_to_string(&mut line)
                .map_err(|e| {
                    io::Error::new(io::ErrorKind::Other,
                                   format!("Can't read entropy from {}, {}", path, e))
                })
                .map(|_| line)
        })
        .and_then(|line| {
            line.trim().parse::<u32>().map_err(|e| {
                io::Error::new(io::ErrorKind::Other,
                               format!("Error parsing entropy_avail: {}", e))
            })
        })
}

fn main() {
    use clap::App;
    let yml = load_yaml!("egd.yaml");
    let m = App::from_yaml(yml).get_matches();

    let fac = slog_syslog::Facility::from_str({
        m.value_of("facility").unwrap()
    });
    let facility;

    match fac {
        Ok(f) => facility = f,
        Err(_) => {
            println!("unable to parse facility: {}",
                     m.value_of("facility").unwrap());
            std::process::exit(1);
        }
    }

    let decorator = slog_term::TermDecorator::new().build();
    let tdrain = slog_term::FullFormat::new(decorator).build().fuse();
    let adrain = slog_async::Async::new(tdrain).build().fuse();

    let sdrain = slog_syslog::unix_3164(facility);

    let sdrain_zz = match sdrain {
        Ok(s) => s,
        Err(e) => {
            println!("unable to open syslog: {}", e);
            std::process::exit(1);
        }
    };

    let log = match m.occurrences_of("syslog") {
        0 => slog::Logger::root(adrain, o!()),
        _ => slog::Logger::root(sdrain_zz.fuse(), o!()),
    };

    let mut allowed_nets: Vec<IPAddress> = vec![];

    if m.occurrences_of("restrict") > 0 {
        // loop through each restrict arg and add them it the array if valid.
        let vals = m.values_of("restrict").unwrap();
        for r in vals {
            let ip = match IPAddress::parse(r) {
                Ok(i) => i,
                Err(e) => {
                    println!("unable to parse ip address {}: {}", r, e);
                    std::process::exit(1);
                }
            };
            allowed_nets.push(ip);
        }
    }

    let mut binding = String::from("127.0.0.1:8888");


    if let Some(arg) = m.value_of("bind") {
        binding = arg.to_string()
    }

    let listener = match TcpListener::bind(&*binding) {
        Ok(v) => v,
        Err(e) => {
            crit!(log, "error creating socket: {}", e.to_string());
            std::process::exit(1);
        }
    };

    info!(log,
          "Entropy server ver {} starting bound to {}",
          env!("CARGO_PKG_VERSION"),
          listener.local_addr().unwrap());

    fn handle_client(mut stream: TcpStream, mut log: slog::Logger) {
        info!(log, "Got connection");
        loop {
            let mut buf = [0; 1];
            let read = match stream.read(&mut buf) {
                Err(e) => {
                    crit!(log, "Got an error at start of loop: {}", e);
                    return;
                }
                Ok(m) => m,
            };
            if read < 1 {
                return;
            }
            let tlog = log.clone();
            match buf[0] {
                EGD_CMD_QUERYPOOL => {
                    // the amount of entropy in the kernal pool is in
                    // /proc/sys/kernel/random/entropy_avail
                    // see random (4) for more info.
                    info!(log, "got querypool");
                    let entropy: u32 = match get_entropy() {
                        Err(e) => {
                            crit!(log, "Got an error when getting entropy: {}", e);
                            return;
                        }
                        Ok(ent) => ent,
                    };
                    let mut wrbuf = vec![];
                    wrbuf.write_u32::<BigEndian>(entropy).unwrap();
                    match stream.write(&wrbuf) {
                        Ok(_) => {}
                        Err(e) => {
                            crit!(log, "Got an error when writing to the client: {}", e);
                            return;
                        }
                    }
                }
                EGD_CMD_READBYTES => {
                    info!(log, "got readbytes");
                    log = log.new(o!("command" => "read"));
                    let read = match stream.read(&mut buf) {
                        Err(e) => {
                            crit!(log, "Got an error: {}", e);
                            return;
                        }
                        Ok(m) => m,
                    };
                    if read < 1 {
                        return;
                    } else {
                        let blocksize = buf[0] as usize;
                        info!(log, "for {} bytes", blocksize);
                        let mut wrbuf = vec![0 as u8; blocksize + 1];
                        // open /dev/urandom
                        let mut randfd = match OpenOptions::new().read(true).open("/dev/urandom") {
                            Ok(f) => f,
                            Err(err) => {
                                error!(log, "Error, unable to open /dev/urandom: {}", err);
                                return;
                            }
                        };
                        wrbuf[0] = blocksize as u8;
                        // XXX non-blocking properly?
                        match randfd.read_exact(&mut wrbuf[1..]) {
                            Ok(s) => s,
                            Err(err) => {
                                error!(log,
                                       "Error, unable to read {} from /dev/urandom: {}",
                                       blocksize,
                                       err);
                                return;
                            }
                        };
                        match stream.write(&wrbuf) {
                            Ok(_) => {}
                            Err(e) => {
                                crit!(log, "Got an error when writing to the client: {}", e);
                                return;
                            }
                        }
                    }
                }
                EGD_CMD_BLOCKREAD => {
                    // This read can block if there isn't enough entropy.
                    // but since we use /dev/urandom there will always be entropy,
                    // so it won't block...
                    log = log.new(o!("command" => "readblocking"));
                    let read = match stream.read(&mut buf) {
                        Err(e) => {
                            crit!(log, "Got an error when reading: {}", e);
                            return;
                        }
                        Ok(m) => m,
                    };
                    if read < 1 {
                        return;
                    } else {
                        let blocksize = buf[0] as usize;
                        info!(log, "read for {} bytes", blocksize);
                        let mut wrbuf = vec![0 as u8; blocksize];
                        // open /dev/urandom
                        let mut randfd = match OpenOptions::new().read(true).open("/dev/urandom") {
                            Ok(f) => f,
                            Err(err) => {
                                error!(log, "Error, unable to open /dev/urandom: {}", err);
                                return;
                            }
                        };
                        match randfd.read_exact(&mut wrbuf) {
                            Ok(s) => s,
                            Err(err) => {
                                error!(log,
                                       "Error, unable to read {} from /dev/urandom: {}",
                                       blocksize,
                                       err);
                                return;
                            }
                        };
                        match stream.write(&wrbuf) {
                            Ok(_) => {}
                            Err(e) => {
                                crit!(log, "Got an error when writing to the client: {}", e);
                                return;
                            }
                        }

                    }
                }
                EGD_CMD_ADDENTROPY => {
                    info!(log, "got addentropy");
                    log = log.new(o!("command" => "addentropy"));
                    // u16 -> bits
                    // u8 -> length
                    // then length worth of bytes
                    let mut bitsbuf = [0; 2];
                    let read = match stream.read(&mut bitsbuf) {
                        Err(e) => {
                            crit!(log, "Got an error when reading bits: {}", e);
                            return;
                        }
                        Ok(m) => m,
                    };
                    if read < 2 {
                        crit!(log, "Got a short read when reading bits");
                        return;
                    } else {
                        let mut rdr = Cursor::new(bitsbuf);
                        let bits = rdr.read_u16::<BigEndian>().unwrap();
                        info!(log, "for {} bits", bits);
                        let mut wrlenbuf = [0; 1];
                        let read = match stream.read(&mut wrlenbuf) {
                            Err(e) => {
                                crit!(log, "Got an error when reading bits: {}", e);
                                return;
                            }
                            Ok(m) => m,
                        };
                        if read < 1 {
                            crit!(log, "Got a short read when reading length");
                            return;
                        }
                        let wrlen = wrlenbuf[0] as usize;
                        info!(log, "for {} bytes of data", wrlen);
                        let mut addbuf = vec![0 as u8; wrlen];
                        let read = match stream.read(&mut addbuf) {
                            Err(e) => {
                                crit!(log, "Got an error when reading bytes: {}", e);
                                return;
                            }
                            Ok(m) => m,
                        };
                        info!(log, "read {} into addbuf", read);
                    }
                }
                EGD_CMD_GETPID => {
                    log = log.new(o!("command" => "getpid"));
                    unsafe {
                        info!(log, "pid: {:?}", libc::getpid());
                    }
                    let pid: i32;
                    unsafe {
                        pid = libc::getpid();
                    };
                    let pidstr: String = pid.to_string();
                    let mut wrbuf = vec![0 as u8; pidstr.len() + 1];
                    wrbuf[0] = pidstr.len() as u8;
                    let mut i = 1;
                    let bytes = pidstr.into_bytes();
                    for b in bytes {
                        wrbuf[i] = b;
                        i += 1;
                    }
                    match stream.write(&wrbuf) {
                        Ok(_) => {}
                        Err(e) => {
                            crit!(log, "Got an error when writing to the client: {}", e);
                            return;
                        }
                    }
                }
                _ => error!(log, "got ??? {:?}", buf[0]),
            }
            log = tlog;
        }
    }

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn({
                    let mut log2 = log.clone();
                    log2 = log2.new(o!("client" => format!("{}",
                                  stream.peer_addr().unwrap()) ));
                    let remote = stream.peer_addr().unwrap().ip().to_string();
                    let remote = IPAddress::parse(remote).unwrap();
                    let doit = match allowed_nets.len() {
                        0 => true,
                        _ => allowed_nets.iter().find(|i| i.includes(&remote)).is_some(),
                    };

                    let log3 = log2.clone();
                    move || {
                        if doit {
                            // it's in one of our nets
                            // connection succeeded
                            handle_client(stream, log2);
                            info!(log3, "conection closed");
                        } else {
                            // denied
                            warn!(log2, "access denied");
                        }
                    }
                });
            }
            Err(e) => {
                // connection failed
                error!(log, "Got an error matching stream: {}", e)
            }
        }
    }

    // close the socket server
    drop(listener);
}
